defmodule Problem_012_Test do
  use ExUnit.Case
  doctest Problem_012

  test "first" do
    assert Problem_012.solve(4, [1, 2]) == [[1, 1, 1, 1], [2, 1, 1], [1, 2, 1], [1, 1, 2], [2, 2]]
  end
end
