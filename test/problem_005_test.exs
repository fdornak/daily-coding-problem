defmodule Problem_005_Test do
  use ExUnit.Case
  doctest Problem_005

  test "car" do
    assert Problem_005.car(Problem_005.cons(3, 4)) == 3
  end

  test "cdr" do
    assert Problem_005.cdr(Problem_005.cons(3, 4)) == 4
  end
end
