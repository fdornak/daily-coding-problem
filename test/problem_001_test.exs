defmodule Problem_001_Test do
  use ExUnit.Case
  doctest Problem_001

  test "first" do
    assert Problem_001.solve([10, 15, 3, 7], 17) == true
  end

  test "first_false" do
    assert Problem_001.solve([1, 2, 3, 18, 19, 20], 24) == false
  end
end
