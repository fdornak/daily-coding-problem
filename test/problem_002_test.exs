defmodule Problem_002_Test do
  use ExUnit.Case
  doctest Problem_002

  test "first" do
    assert Problem_002.solve([1, 2, 3, 4, 5]) == [120, 60, 40, 30, 24]
  end

  test "second" do
    assert Problem_002.solve([3, 2, 1]) == [2, 3, 6]
  end

  test "third" do
    assert Problem_002.solve([3, 1, 2, 4]) == [8, 24, 12, 6]
  end

  test "first_2" do
    assert Problem_002.solve2([1, 2, 3, 4, 5]) == [120, 60, 40, 30, 24]
  end

  test "second_2" do
    assert Problem_002.solve2([3, 2, 1]) == [2, 3, 6]
  end

  test "third_2" do
    assert Problem_002.solve2([3, 1, 2, 4]) == [8, 24, 12, 6]
  end
end
