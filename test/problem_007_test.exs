defmodule Problem_007_Test do
  use ExUnit.Case
  doctest Problem_007

  test "11" do
    assert Problem_007.solve("11") == 2
  end

  test "111" do
    assert Problem_007.solve("111") == 3
  end

  test "1111" do
    assert Problem_007.solve("1111") == 5
  end

  test "81" do
    assert Problem_007.solve("81") == 1
  end

  test "1311" do
    assert Problem_007.solve("1311") == 4
  end
end
