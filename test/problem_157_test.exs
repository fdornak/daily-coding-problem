defmodule Problem_157_Test do
  use ExUnit.Case
  doctest Problem_157

  test "first" do
    assert Problem_157.solve("racecar") == true
  end

  test "second" do
    assert Problem_157.solve("carrace") == true
  end

  test "third" do
    assert Problem_157.solve("daily") == false
  end
end
