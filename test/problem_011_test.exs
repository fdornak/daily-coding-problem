defmodule Problem_011_Test do
  use ExUnit.Case
  doctest Problem_011

  test "first" do
    assert Problem_011.solve("de", ["dog", "deer", "deal"]) == ["deer", "deal"]
  end
end
