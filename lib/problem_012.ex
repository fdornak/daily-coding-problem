defmodule Problem_012 do
  @moduledoc """
  This problem was asked by Amazon.

  There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time. Given N, write a function that returns the number of unique ways you can climb the staircase. The order of the steps matters.

  For example, if N is 4, then there are 5 unique ways:

  1, 1, 1, 1
  2, 1, 1
  1, 2, 1
  1, 1, 2
  2, 2
  What if, instead of being able to climb 1 or 2 steps at a time, you could climb any number from a set of positive integers X? For example, if X = {1, 3, 5}, you could climb 1, 3, or 5 steps at a time.
  """

  def solve(steps, options) do
    _solve(steps, options, [])
  end

  defp _solve(remainingSteps, options, solution) when remainingSteps > 0 do
    Enum.flat_map(options, fn option ->
      cond do
        remainingSteps - option >= 0 ->
          _solve(remainingSteps - option, options, [option | solution])

        true ->
          []
      end
    end)
  end

  defp _solve(_, _, solution) do
    [solution]
  end
end
