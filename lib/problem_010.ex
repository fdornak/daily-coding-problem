defmodule Problem_010 do
  @moduledoc """
  This problem was asked by Apple.

  Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds.
  """
  def solve(f, n) do
    Process.spawn(fn -> run_after(f, n) end, [:monitor])
  end

  def run_after(f, n) do
    :timer.sleep(n)
    f.()
  end
end
