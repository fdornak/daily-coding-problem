defmodule Problem_014 do
  @moduledoc """
  This problem was asked by Google.

  The area of a circle is defined as πr^2. Estimate π to 3 decimal places using a Monte Carlo method.

  Hint: The basic equation of a circle is x2 + y2 = r2.
  """

  def solve(pointCount) do
    monteCarloCircleArea(pointCount, 30)
  end

  defp monteCarloCircleArea(pointCount, r) do
    inside =
      Enum.reduce(0..pointCount, 0, fn _, acc ->
        # x^2 + y^2 < r^2
        if abs(
             :math.pow(:rand.uniform() * 2 * r - r, 2) + :math.pow(:rand.uniform() * 2 * r - r, 2)
           ) <= :math.pow(r, 2) do
          acc + 1
        else
          acc
        end
      end)

    # x^2 * (inside/total) = S | S / r^2 = pi
    :math.pow(2 * r, 2) * (inside / pointCount) / :math.pow(r, 2)
  end
end
