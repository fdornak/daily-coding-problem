defmodule Problem_001 do
  @moduledoc """
  This problem was recently asked by Google.

  Given a list of numbers and a number k, return whether any two numbers from the list add up to k.

  For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.

  Bonus: Can you do this in one pass?
  """

  def solve(numbers, k) do
    x = solve(numbers, k, MapSet.new())
    x
  end

  defp solve([head | tail], k, accepted_numbers) do
    if Enum.member?(accepted_numbers, head) do
      true
    else
      accepted_numbers = MapSet.put(accepted_numbers, k - head)
      solve(tail, k, accepted_numbers)
    end
  end

  defp solve(_, _, _) do
    false
  end
end
