defmodule Problem_005 do
  @moduledoc """
  This problem was asked by Jane Street.

  cons(a, b) constructs a pair, and car(pair) and cdr(pair) returns the first and last element of that pair. For example, car(cons(3, 4)) returns 3, and cdr(cons(3, 4)) returns 4.

  Given this implementation of cons:

  def cons(a, b):
    def pair(f):
      return f(a, b)
    return pair

  Implement car and cdr.
  """

  def cons(a, b) do
    & &1.(a, b)
  end

  def car(fun) do
    fun.(fn x, _ -> x end)
  end

  def cdr(fun) do
    fun.(fn _, x -> x end)
  end
end
