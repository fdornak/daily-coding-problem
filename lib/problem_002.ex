defmodule Problem_002 do
  @moduledoc """
   This problem was asked by Uber.

   Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.

   For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].

   Follow-up: what if you can't use division?
  """

  def solve(numbers) do
    total_product = Enum.reduce(numbers, &(&1 * &2))
    Enum.map(numbers, &(total_product / &1))
  end

  def solve2(numbers) do
    bottom_up = stepped_product(numbers, 1)
    top_down = Enum.reverse(stepped_product(Enum.reverse(numbers), 1))
    Enum.zip(bottom_up, top_down) |> Enum.map(fn {a, b} -> a * b end)
  end

  defp stepped_product([head | tail], accum) when head != nil do
    new_product = accum * head
    [accum | stepped_product(tail, new_product)]
  end

  defp stepped_product(_, _) do
    []
  end
end
