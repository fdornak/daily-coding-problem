defmodule Problem_007 do
  @moduledoc """
  This problem was asked by Facebook.

  Given the mapping a = 1, b = 2, ... z = 26, and an encoded message, count the number of ways it can be decoded.

  For example, the message '111' would give 3, since it could be decoded as 'aaa', 'ka', and 'ak'.

  You can assume that the messages are decodable. For example, '001' is not allowed.
  """
  def solve(message) do
    string_to_integer_list(message) |> get_options
  end

  defp string_to_integer_list(string) do
    String.codepoints(string) |> Enum.map(&String.to_integer(&1))
  end

  defp get_options([a, b | tail]) when a * 10 + b <= 26 do
    get_options([b | tail]) + get_options(tail)
  end

  defp get_options([_ | tail]) do
    get_options(tail)
  end

  defp get_options(_) do
    1
  end
end
