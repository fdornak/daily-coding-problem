defmodule Problem_157 do
  @moduledoc """
  This problem was asked by Amazon.

  Given a string, determine whether any permutation of it is a palindrome.

  For example, carrace should return true, since it can be rearranged to form racecar, which is a palindrome. daily should return false, since there's no rearrangement that can form a palindrome.
  """
  @spec solve(String.t()) :: boolean
  def solve(word) do
    can_palindrome?(word)
  end

  defp can_palindrome?(word) do
    char_count =
      word
      |> String.graphemes()
      |> Enum.reduce(%{}, fn char, acc ->
        Map.put(acc, char, (acc[char] || 0) + 1)
      end)

    one_single_character =
      char_count
      |> Enum.filter(fn {_, count} -> count == 1 end)
      |> Enum.count() == 1

    rest_are_pairs =
      char_count
      |> Enum.filter(fn {_, count} -> count != 1 end)
      |> Enum.all?(fn {_, count} -> rem(count, 2) == 0 end)

    one_single_character and rest_are_pairs
  end
end
